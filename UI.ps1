Using module .\Class.psm1
Using module .\Functions.psm1
.\Keepass.ps1

#Environment
$global:DictionaryPath = ((get-location).Path + "\Class.secret")
$global:GeneralPasswordPath = ((get-location).Path + "\Password")
$global:KeePassDatabasePath = ((get-location).Path + "\Database.kdbx")
$global:KPScriptPath = "C:\Program Files (x86)\KeePass Password Safe 2\KPScript.exe"


#If General password is not exist
if ((Get-Content $global:GeneralPasswordPath) -eq $Null) {
      Set-GeneralPassword
      Import-Dictionary -Path $global:DictionaryPath
}
else{
    #Input General password
    $global:InputPassword = read-host "Please enter password" -AsSecureString

    #Loading General password from file
    $DictionaryPassword = (Get-Content -Path $global:GeneralPasswordPath)

    #Checking of entered password
    $CheckPassword = [System.Text.Encoding]::ASCII.GetString((Generate-EncryptionKey -Password (Get-Password $global:InputPassword) ))
    if($DictionaryPassword -eq $CheckPassword){
    "Password is correct"
    Import-Dictionary -Path $global:DictionaryPath
    }
    else{"Password is wrong"}
}
