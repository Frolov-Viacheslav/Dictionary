Using module .\Class.psm1


Function global:Get-SecretValue{
      <#
        .SYNOPSIS
            Gets value of secret by title.
        .DESCRIPTION
            The Get-Secret function gets values of secrets which match the value of the entered title. 
            The function returns value of secret. 
        .PARAMETER Title
            Specifies the title of secret.  
        .PARAMETER KeePass
            Specifies the output from KeePass database.   
        .EXAMPLE
            PS> Get-SecretValue -Title "Password for Google"
        .EXAMPLE
            PS> Get-SecretValue -Title "Password for Google" -KeePass
        .LINK
            Get-SecretList, Add-Secret, Edit-Secret, Remove-Secret, Expand-ExpiresTime, Add-Tag, Generate-Password, Restore-Password, Get-PasswordHistory
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
      #>
      [CmdletBinding()]
      param (
      [Parameter(Mandatory = $true)]
      [String] $Title,
      [Switch] $KeePass
      )
      #Output from Dictionary database
      if(!$KeePass){
          $Password = $global:Dictionary | where {$_.Title -eq $Title}
          if($Password -ne $null){
            Get-Password $Password.SecretValue
          }
          else{
            'There is no secret with title "' + $Title + '"'
          }
      }
      #Otput from KeePass database
      else{
          Get-KPSecretValue -Title $Title
      }
    }

Function global:Get-SecretList{
      <#
        .SYNOPSIS
            Gets list of secret by title or name or URL or expires time.
        .DESCRIPTION
            The Get-SecretList function gets list of secrets which match the value of the entered title or name or URL or expires time in days. 
            The function returns list of suitable secrets. 
        .PARAMETER Title
            Specifies the title of secret.
        .PARAMETER Name
            Specifies the name/login of secret. 
        .PARAMETER URL
            Specifies the URL of secret.
        .PARAMETER Days
            Specifies the expires time of secret in days.  
        .PARAMETER KeePass
            Specifies the output from KeePass database.  
        .EXAMPLE
            PS> Get-SecretList -Title "Password for Google"
            Title       : Password for Google
            SecretType  : credential
            Name        : Name
            SecretValue : System.Security.SecureString
            URL         : www.g.co
            Tag         : {google}
            ExpiresTime : 9/19/2019 10:53:08 PM
        .EXAMPLE
            PS> Get-SecretList -Days 30
            Title       : Token for Yandex
            SecretType  : token
            Name        : root
            SecretValue : System.Security.SecureString
            URL         : www.yandex.ru
            Tag         : {Root token}
            ExpiresTime : 9/18/2019 10:52:39 PM
        .EXAMPLE
            PS> Get-SecretList -Name "root"
            Title       : Token for Yandex
            SecretType  : token
            Name        : root
            SecretValue : System.Security.SecureString
            URL         : www.yandex.ru
            Tag         : {Root token}
            ExpiresTime : 9/18/2019 10:52:39 PM  

        .EXAMPLE
            PS> Get-SecretList -URL "www.google.com"
        .LINK
            Get-SecretValue, Add-Secret, Edit-Secret, Remove-Secret, Expand-ExpiresTime, Add-Tag, Generate-Password, Restore-Password, Get-PasswordHistory
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov 
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary     
      #>
      [CmdletBinding()]
      param (
      [String] $Title,
      [String] $Name,
      [String] $URL,
      [int] $Days,
      [Switch] $KeePass
      )
      #Output from Dictionary database
      if(!$KeePass){
        $global:Dictionary | where {($_.Title -eq $Title) -or ($_.Name -eq $Name) -or ($_.URL -eq $URL) -or ((($_.ExpiresTime - (Get-Date)).Days) -eq $Days)}
      }
      #Output from KeePass database
      else{
        Get-KPSecretList -Title $Title -Name $Name -URL $URL
      }
    }

Function global:Edit-Secret{
      <#
        .SYNOPSIS
            Edits secret by title.
        .DESCRIPTION
            The Edit-Secret function edits name, expires time, value of secret, URL, tag which match the value of the entered ones. 
            You have to input number of days on which expires time will be set. 
            You have to specifies parameter "ChangeSecretValue" if you want to change value of secret.
            The function returns edited secret.
        .PARAMETER Title
            Specifies the title of secret.
        .PARAMETER NewName
            Specifies the name/login of secret. 
        .PARAMETER ChangeSecretValue
            If it specifies request of new value of secret will be send. 
        .PARAMETER NewURL
            Specifies the URL of secret. 
        .PARAMETER NewDays
            Specifies the expires of days. 
        .PARAMETER NewTag
            Specifies the tags of secret. 
        .PARAMETER Path
            Specifies the path to location of Dictionary.
        .EXAMPLE
            PS> Edit-Secret -Title "Password for Google" -NewName "Name" -ChangeSecretValue -NewURL "www.g.co" -NewDays 30 -NewTag "google"
            Title       : Password for Google
            SecretType  : credential
            Name        : Name
            SecretValue : System.Security.SecureString
            URL         : www.g.co
            Tag         : {google}
            ExpiresTime : 9/19/2019 10:53:08 PM
         .EXAMPLE
            PS> Edit-Secret -Title "Password for Google" -NewName "Petr"
            Title       : Password for Google
            SecretType  : credential
            Name        : Petr
            SecretValue : System.Security.SecureString
            URL         : www.g.co
            Tag         : {google}
            ExpiresTime : 9/19/2019 10:53:08 PM
         .LINK
            Get-SecretList, Get-SecretValue, Add-Secret, Remove-Secret, Expand-ExpiresTime, Add-Tag, Generate-Password, Restore-Password, Get-PasswordHistory
         .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
      #>
      [CmdletBinding()]
      param (
      [Parameter(Mandatory = $true)]
      [String] $Title,
      [String] $NewName,
      [Switch] $ChangeSecretValue,
      [String] $NewURL,
      [int] $NewDays,
      [String[]] $NewTag,
      [String] $Path = $global:DictionaryPath
      )
      #Get Secret by Title
      $Secret = Get-SecretList -Title $Title
      if($Secret -ne $null){
          #Edit Expires time
          if($NewDays){
              $Secret | %{$_.ExpiresTime = (Get-Date).AddDays($NewDays)}
          } 
          #Edit Secret value
          if($ChangeSecretValue){
              $Secret | %{$_.SecretValue = Read-Host -Prompt "Enter Secret value" -AsSecureString;
                          #Add record to the history of passwords
                          if($_.SecretType -eq "credential"){
                            $_.PasswordHistory += $_.SecretValue    
                          }
                        }
          }
          #Edit Tag
          if($NewTag){
              $Secret | %{$_.Tag = $NewTag}
          }
          #Edit Name
          if($NewName){
              $Secret | %{$_.Name = $NewName}
          }
          #Edit URL
          if($NewURL){
              $Secret | %{$_.URL = $NewURL}
          }
          #Print edited secret
          $Secret
          Save-Dictionary -Path $Path
          #KeePass
          Edit-KPSecret -Title $Title -Name $Secret.Name -SecretValue (Get-Password $Secret.SecretValue) -URL $Secret.URL -ExpiresTime $Secret.ExpiresTime | out-null
      }
      else{
          'There is no secret with title "' + $Title + '"'
      }
    }
Function global:Remove-Secret{
      <#
        .SYNOPSIS
            Removes secret by title.
        .DESCRIPTION
            The Remove-Secret function removes secret which match the value of the entered title.  
        .PARAMETER Title
            Specifies the name/login of secret.  
        .PARAMETER Path
            Specifies the path to location of Dictionary. 
        .PARAMETER Force
            Supresses all confirmation prompts. 
        .EXAMPLE
            PS> Remove-Secret -Name "root"
        .LINK
            Get-SecretList, Add-Secret, Edit-Secret, Get-SecretValue, Expand-ExpiresTime, Add-Tag, Generate-Password, Restore-Password, Get-PasswordHistory
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
      #>
      [CmdletBinding(
           ConfirmImpact = 'Medium',
           SupportsShouldProcess = $true
      )]
      param (
      [Parameter(Mandatory = $true)] 
      [String] $Title,
      [String] $Path = $global:DictionaryPath,
      [Switch] $Force
      )
      #Confirm removing secret from Dictionary
      if ($Force -or $PSCmdlet.ShouldProcess($Title,"Remove secret")) {
      $global:Dictionary = @($global:Dictionary | where {$_.Title -ne $Title})
      Save-Dictionary -Path $Path
      }
      #Confirm removing from KeePass database
      if ($Force -or $PSCmdlet.ShouldProcess($Title,"Remove secret from KeePass database")) {
      #KeePass
      Remove-KPSecret -Title $Title | out-null
      }
    }


Function global:Generate-Password{
      <#
        .SYNOPSIS
            Generates password by pattern.
        .DESCRIPTION
            The Generate-Password function generates password by the value of the entered pattern.
            You can use this password as secret value if secret is entered. 
            The function returns password or edited secret if secret is entered. 
        .PARAMETER Pattern
            Specifies pattern of password. 
        .PARAMETER Secret
                Specifies the secret.
        .PARAMETER Path
            Specifies the path to location of Dictionary.  
        .EXAMPLE
            PS> Generate-Password -Pattern "#30ADa%D10A10a5DDA"
            7904803519UIKNWVVRNLaipid86SdT
        .EXAMPLE
            PS> Get-SecretList -Title "Password for Google" | Generate-Password "#20D"
            Title       : Password for Google
            SecretType  : credential
            Name        : Petr
            SecretValue : System.Security.SecureString
            URL         : www.g.co
            Tag         : {google}
            ExpiresTime : 9/19/2019 10:53:08 PM
        .LINK
            Get-SecretValue, Get-SecretList, Add-Secret, Edit-Secret, Remove-Secret, Expand-ExpiresTime, Add-Tag, Restore-Password, Get-PasswordHistory
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
      #>
      [CmdletBinding()]
      param (
      [Parameter(Mandatory = $true)]
       [ValidateScript({
        if( $_ -match "^#[0-9]{1,}[AaD]{1,3}%([AaD][0-9]{0,}){1,}$") {
            $true
        }
        elseif( $_ -match "^#[0-9]{1,}[AaD]{1,3}$"){
            $true
        }
        else
        {
            throw 'Please provide a valid pattern: #number(AaD)%(AaD)number. For example: #20aAD%A5, #20D'
        }
        })]
      [String] $Pattern,
      [Parameter(ValueFromPipeline = $true)]
      [Secret] $Secret,
      [String] $Path = $global:DictionaryPath
      )
      #Get random symbol with match pattern (A,a,D)
      Function RandomContent([String] $Content){
            if($Content -cmatch "A"){
                $random += (65..90)
            } 
            if($Content -cmatch "a"){
                $random += (97..122)
            } 
            if($Content -cmatch "D"){
                $random += (48..57)
            }
            return $random
       }
      #Parsing password template
      $amount = (([regex]::matches($Pattern, "^#[0-9]{1,}")).Value).Trim("#") #Calculation of amount of password
      if($Pattern -match "%"){
        $content = (([regex]::matches($Pattern, "[a-zA-Z]{1,}%")).Value).Trim("%") #Getting content of password 
        $format = (([regex]::matches($Pattern, "%[a-zA-Z0-9]{1,}")).Value).Trim("%") # Getting of format string 
        $groups = ([regex]::matches($format, "([AaD][0-9]{1,})|([AaD](?![0-9]))")).Value #Divided format string by groups
        #Generation of format password part 
        $groups | foreach{
            $random = RandomContent($_[0]);
            if($_[1] -notmatch "[0-9]") {
                $c = 1
            } 
            else {
                $c = $_.Remove(0,1)
            };
            $length = [convert]::ToInt32($c, 10);
            for($i=0; $i -lt $length; $i++){$pass +=  ($random | Get-Random  | %{[char]$_})} ;
        }
      }
      else{
        $content = ([regex]::matches($Pattern, "[a-zA-Z]{1,}$")).Value
      }
      #Generation of remaining password
      $remaining = ($amount - $pass.length)
      if($remaining -ne 0){
        $random = RandomContent($content)
        for($i=0; $i -lt $remaining; $i++){$pass += ($random | Get-Random | % {[char]$_})}
      }
      #Change password of secret
      if($Secret){
        $Secret.SecretValue = ($pass | ConvertTo-SecureString -AsPlainText -Force)
        #Add record to the history of passwords
        $Secret.PasswordHistory += $Secret.SecretValue
        #Print edited secret
        $Secret
        Save-Dictionary -Path $Path
        #KeePass
        Edit-KPSecret -Title $Secret.Title -Name $Secret.Name -SecretValue $pass -URL $Secret.URL -ExpiresTime $Secret.ExpiresTime | out-null
      }
      else{
        #Print generated password
        return $pass
      }
    }

Function global:Add-Secret{
       <#
            .SYNOPSIS
                Adds secret to Dictionary.
            .DESCRIPTION
                The Add-Secret function adds entered secret to Dictionary.
                You have to enter all fields of secret. However, SecretType and Days have default values. 
                Also Tag is empty by default. Function returns new secret.
            .PARAMETER Title
                Specifies the title of secret. 
            .PARAMETER SecretType
                Specifies the type of secret (credential, token, SSH_key).
            .PARAMETER Name
                Specifies the name of secret.
            .PARAMETER URL
                Specifies the URL of secret.
            .PARAMETER Tag
                Specifies Tags of secret.
            .PARAMETER Days
                Specifies the expires time os secret in days.
            .PARAMETER Path
            Specifies the path to location of Dictionary.  
            .EXAMPLE
                PS> Add-Secret -Title "Add Secret" -SecretType SSH_key -Name "Name" -URL "Test.com" -Tag "Test" -Days "30"
                Title       : Add Secret
                SecretType  : SSH_key
                Name        : Name
                SecretValue : System.Security.SecureString
                URL         : Test.com
                Tag         : {Test}
                ExpiresTime : 9/20/2019 12:38:06 AM
            .EXAMPLE
                PS> Add-Secret -Title "Default" -Name "Name" -URL "Test.com"
                Title       : Default
                SecretType  : credential
                Name        : Name
                SecretValue : System.Security.SecureString
                URL         : Test.com
                Tag         : {}
                ExpiresTime : 9/20/2019 12:39:16 AM
            .LINK
                Get-SecretList, Get-SecretValue, Edit-Secret, Remove-Secret, Expand-ExpiresTime, Add-Tag, Generate-Password, Restore-Password, Get-PasswordHistory
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
       #>
       [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true)]
        [String] $Title,
        [ValidateSet('credential', 'token', 'SSH_key')]
        [String] $SecretType = "credential",
        [Parameter(Mandatory = $true)]
        [String] $Name,
        [Parameter(Mandatory = $true)]
        [String] $URL,
        [String[]] $Tag = @(),
        [int] $Days = 30,
        [String] $Path = $global:DictionaryPath
        )
        #User input secret value as secure string (*********)
        $SecretValue = Read-Host -Prompt "Enter Secret value" -AsSecureString
        #Create new object
        $Secret = New-Object Secret(
            $Title,
            $SecretType,
            $Name,
            $SecretValue,
            $URL,
            $Tag,
            $Days
        )
        #Print new secret
        $Secret
        $global:Dictionary += $Secret
        Save-Dictionary -Path $Path
        #KeePass
        Add-KPSecret -Title $Title -Name $Name -SecretValue (Get-Password $SecretValue) -URL $URL -Days $Days | out-null
    }
Function global:Expand-ExpiresTime{
       <#
            .SYNOPSIS
                Extends expires time of secret on some days.
            .DESCRIPTION
                The Expand-ExpiresTime function extends expires time on the value of the entered days. 
                The function returns edited secret. 
            .PARAMETER Secret
                Specifies the secret.   
            .PARAMETER Day
                Specifies number of days.  
            .PARAMETER Path
                Specifies the path to location of Dictionary.
            .EXAMPLE
                PS> Get-SecretList -Title "Password for Google" | Expand-ExpiresTime -Day 50
                Title       : Password for Google
                SecretType  : credential
                Name        : Petr
                SecretValue : System.Security.SecureString
                URL         : www.g.co
                Tag         : {google}
                ExpiresTime : 11/8/2019 10:53:08 PM
            .LINK
                Get-SecretList, Add-Secret, Edit-Secret, Remove-Secret, Get-SecretValue, Add-Tag, Generate-Password, Restore-Password, Get-PasswordHistory
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true)]        
        [int] $Days,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Secret] $Secret,
        [String] $Path = $global:DictionaryPath
        )
        #Add entered count of days to expires time of secret
        $Secret.ExpiresTime = $Secret.ExpiresTime.AddDays($Days)
        #Print edited secret
        $Secret
        Save-Dictionary -Path $Path
        #KeePass
        Edit-KPSecret -Title $Secret.Title -ExpiresTime $Secret.ExpiresTime -Name $Secret.Name -SecretValue (Get-Password $Secret.SecretValue) -URL $Secret.URL | out-null    
    }
Function global:Add-Tag{
        <#
            .SYNOPSIS
                Adds new tags to secret.
            .DESCRIPTION
                The Add-Tag function adds new tags to the entered secret. 
                The function returns edited secret. 
            .PARAMETER Secret
                Specifies the secret.
            .PARAMETER NewTag
                Specifies the new tags. 
            .PARAMETER Path
                Specifies the path to location of Dictionary.   
            .EXAMPLE
                PS> $Dictionary[0] | Add-Tag -NewTag ("Tag 1", "Tag 2") -Path ".\Class.secret"
                SecretType  : credential
                Name        : Slava
                SecretValue : 444
                URL         : www.google.com
                Tag         : {My password, Google, Tag 1, Tag 2}
                ExpiresTime : 1/24/2022 10:58:12 PM
            .LINK
                Get-SecretList, Get-Secretvalue, Add-Secret, Edit-Secret, Remove-Secret, Expand-ExpiresTime, Generate-Password, Restore-Password, Get-PasswordHistory
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true)] 
        [String[]]$NewTag,
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Secret] $Secret,
        [String] $Path = $global:DictionaryPath
        )
        $Secret.Tag += $NewTag
        #Print edited secret
        $Secret
        Save-Dictionary -Path $Path
    }

Function global:Restore-Password{
        <#
            .SYNOPSIS
                Restores previously used password.
            .DESCRIPTION
                The Restore-Password function restores password  from the list of previously used passwords index of which is entered. 
                The function returns new secret value. 
            .PARAMETER Secret
                Specifies the secret.
            .PARAMETER Index
                Specifies the index of previously password.    
            .EXAMPLE
                PS> Get-SecretList -Title "Password for Google" | Restore-Password -Index 2
            .LINK
                Get-SecretValue, Get-SecretList, Add-Secret, Edit-Secret, Remove-Secret, Expand-ExpiresTime, Add-Tag, Get-PasswordHistory, Generate-Password
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary

        #>
        [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Secret] $Secret,
        [Parameter(Mandatory = $true)]
        [int] $Index,
        [String] $Path = $global:DictionaryPath
        )
        #Search the entered index of password in history of passwords and restore it
        $Secret.SecretValue = $Secret.PasswordHistory[$Index - 1]
        Get-Password $Secret.SecretValue
        #Add record to the history of passwords
        $Secret.PasswordHistory += $Secret.SecretValue
        Save-Dictionary -Path $Path
        #KeePass
        Edit-KPSecret -Title $Secret.Title -ExpiresTime $Secret.ExpiresTime -Name $Secret.Name -SecretValue (Get-Password $Secret.SecretValue) -URL $Secret.URL | out-null
}
Function global:Get-PasswordHistory{
        <#
            .SYNOPSIS
                Gets password history of secret.
            .DESCRIPTION
                The Get-PasswordHistory function gets password history of the entered secret. 
                The function returns list of previously passwords. 
            .PARAMETER Secret
                Specifies the secret.  
            .EXAMPLE
                PS> $Dictionary[0] | Get-PasswordHistory
            .LINK
                Get-SecretValue, Get-SecretList, Add-Secret, Edit-Secret, Remove-Secret, Expand-ExpiresTime, Add-Tag, Restore-Password, Generate-Password
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary

        #>
        [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [Secret] $Secret
        )
        #index of password history
        $Index = 0
        foreach ($Password in $Secret.PasswordHistory){
            $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($Password)
            $PlainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
            $Index++
            Write-Host $Index"." $PlainPassword 
            
        }
        
}
Function global:Import-Dictionary{
        <#
            .SYNOPSIS
                Imports Dictionary from file.
            .DESCRIPTION
                The Import-Dictionary function imports Dictionary from the entered file.
                All objects in file get type Secret. Their values of properties are copied to properties of Secret.    
            .PARAMETER Path
                Specifies the path of file.  
            .EXAMPLE
                PS> Import-Dictionary -Path ".\Class.secret"
             .LINK
                Encrypt-Dictionary, Decrypt-Dictionary, Generate-EncryptionKey
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param(
        [String] $Path = $global:DictionaryPath
        )
        $global:Dictionary = @()
        if ((Get-Content $Path) -eq $Null) {
            $Credential = New-Object Secret("Password for Google", "credential", "Slava", ("444" | ConvertTo-SecureString -AsPlainText -Force), "www.google.com", ("My password", "Google"), 30)
            $Token = New-Object Secret("Token for Yandex", "token", "root", ("d50d9fd00acf797ac409d5890fcc76669b727e63" | ConvertTo-SecureString -AsPlainText -Force), "www.yandex.ru", ("Root token"), 30)
            $SSH_key = New-Object Secret("Key for telegram", "SSH_key", "Slava", ("ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAukDVDC9cSktXTBKVxM1Ddaa6RHkDefRyHGaR1bcbFUVFlASXTMx8SvzSdoRUgQhNF9rKuAiAWCliZbYJLMd97jnQHk1Bq1yFSpxEFmVQWt+/fTp500QH5uBYY0T/6sj1okM/txDr/GHogIkM4lUDsfXn6VhYkgEebx2MtkdGj2yH2aod8OJdeCMgVvj/n5zpETqVX0WsNqv9UbSpEwj8Q2U8puMqnS74hyxBEjiGdiBY1i4sAVgg1NUXfWLNfZ0BoFiOQ+2lCxEUrMz+M29eqm+WWCc6vTrhel0bIgt/vTcEi8xx1rjGOb077ASd7uTBlRqIDnnvbidNh9/mpNgPpQ==" | ConvertTo-SecureString -AsPlainText -Force), "www.telegram.com", ("My ssh key"), 30)
            $global:Dictionary += $Credential
            $global:Dictionary +=$Token
            $global:Dictionary += $SSH_key
            Save-Dictionary -Path $Path
        }
        else{
            Decrypt-Dictionary -Password $global:InputPassword
            $global:Dictionary += Import-Clixml -Path $Path | %{ New-Object Secret($_.Title, $_.SecretType, $_.Name, $_.SecretValue, $_.URL, $_.Tag, (($_.ExpiresTime - (Get-Date)).Days), @($_.PasswordHistory))}
            Encrypt-Dictionary -Password $global:InputPassword
        }
}

#Security
Function Initialize-AES256{
    param(
    [String] $Password
    )
    $AES256 = New-Object System.Security.Cryptography.AesManaged
    $AES256.KeySize = 256
    $Key = Generate-EncryptionKey -Password $Password
    $AES256.Key = $Key
    $AES256.Padding = [System.Security.Cryptography.PaddingMode]::Zeros
    $AES256.Mode = [System.Security.Cryptography.CipherMode]::CBC
    return $AES256
}
Function global:Encrypt-Dictionary{
        <#
            .SYNOPSIS
                Encrypts file with secrets.
            .DESCRIPTION
                The Encrypt-Dictionary function encrypts file with secrets from the entered path.
                Encryption key is got from entered password.
                Encryption algorithm is AES, key size = 256, mode = CBC, padding = Zeroes, block size = 128.    
            .PARAMETER Path
                Specifies the path of file.
            .PARAMETER Password
                Specifies the password for encryption key.  
            .EXAMPLE
                PS> Encrypt-Dictionary  -Password "123"
            .LINK
                Decrypt-Dictionary, Generate-EncryptionKey
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
         [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Password,
        [String] $Path = $global:DictionaryPath
        )
        $AES256 = Initialize-AES256 $Password
        #Encription of file
        $File = Get-Item -Path $Path
        $plainBytes = [System.IO.File]::ReadAllBytes($File.FullName)
        $Encryptor = $AES256.CreateEncryptor()
        $encryptedBytes = $Encryptor.TransformFinalBlock($plainBytes, 0, $plainBytes.Length)
        $encryptedBytes = $AES256.IV + $encryptedBytes
        [System.IO.File]::WriteAllBytes($Path, $encryptedBytes)
}
Function global:Decrypt-Dictionary{
        <#
            .SYNOPSIS
                Decrypts file with secrets.
            .DESCRIPTION
                The Decrypt-Dictionary function encrypts file with secrets from the entered path. 
                Encryption key is got from entered password.
                Encryption algorithm is AES, key size = 256, mode = CBC, padding = Zeroes, block size = 128.    
            .PARAMETER Path
                Specifies the path of file.
            .PARAMETER Password
                Specifies the password for encryption key.  
            .EXAMPLE
                PS> Decrypt-Dictionary -Password "123"
            .LINK
                Encrypt-Dictionary, Generate-EncryptionKey
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Password,
        [String] $Path = $global:DictionaryPath
        )
        $AES256 = Initialize-AES256 $Password
        #Decription of file
        $File = Get-Item -Path $Path
        $cipherBytes = [System.IO.File]::ReadAllBytes($File.FullName)
        $AES256.IV = $cipherBytes[0..15]
        $Decryptor = $AES256.CreateDecryptor()
        $decryptedBytes = $Decryptor.TransformFinalBlock($cipherBytes, 16, $cipherBytes.Length - 16)
        
        $DecryptedString = [System.Text.Encoding]::UTF8.GetString($decryptedBytes).Trim([char]0)

        [System.IO.File]::WriteAllText($Path, $DecryptedString)
}
Function global:Generate-EncryptionKey{
        <#
            .SYNOPSIS
                Generates key for AES-256.
            .DESCRIPTION
                The Generate-EncryptionKey function generates key for AES-256 using the entered password. 
                Hash SHA-256 is computed from the entered password
                The function returns array of bytes as encryption key.    
            .PARAMETER Password
                Specifies the password for encryption key.  
            .EXAMPLE
                PS> Generate-EncryptionKey -Password "123"
            .LINK
                Encrypt-Dictionary, Decrypt-Dictionary
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [String] $Password
        )
        $SHA256 = New-Object System.Security.Cryptography.SHA256Managed
        $Key = $SHA256.ComputeHash([System.Text.Encoding]::UTF8.GetBytes($Password))
        #$Key = [System.Text.Encoding]::UTF8.GetString($Key)
        return $Key
}

Function global:Set-GeneralPassword{
        <#
            .SYNOPSIS
                Sets general password for file of secrets.
            .DESCRIPTION
                Set-GeneralPassword function sets general password if it does not exist.
                You have to input it as secure string (***).
                Password is hashed and written to the file.
            .EXAMPLE
                PS> Set-GeneralPassword
            .LINK
                Edit-GeneralPassword
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param()
        #Request General password
        $GeneralPassword = read-host "Please set General password" -AsSecureString
        #Writing hash of password to file
        Set-Content -Value ([System.Text.Encoding]::ASCII.GetString((Generate-EncryptionKey -Password (Get-Password $GeneralPassword) ))) -Path $global:GeneralPasswordPath
        #Encrypt-Dictionary -Password $GeneralPassword
        $global:InputPassword = $GeneralPassword
        #KeePass
        $result = Edit-KPGeneralPassword  $GeneralPassword
        #General password must be the idential in Dictionary and KeePass
        if($result -eq "OK: Operation completed successfully."){
            "Master key of KeePass database is correct"
        }
        else{
            "Change your Master key of KeePass database"
        }
            
}

Function global:Edit-GeneralPassword{
          <#
            .SYNOPSIS
                Edits general password for file of secrets.
            .DESCRIPTION
                Edit-GeneralPassword function edits general password on value entered by user.
                You have to input it as secure string (***).
                Password is hashed and written to the file.
            .EXAMPLE
                PS> Edit-GeneralPassword
            .LINK
                Set-GeneralPassword
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param()
        #Request for new General password
        $GeneralPassword = read-host "Please enter new General password" -AsSecureString
        #Decription of file with secrets
        Decrypt-Dictionary -Password $global:InputPassword
        #Writing new General password to file
        Set-Content -Value ([System.Text.Encoding]::ASCII.GetString((Generate-EncryptionKey -Password (Get-Password $GeneralPassword) ))) -Path $global:GeneralPasswordPath
        #Encription of file with secrets
        Encrypt-Dictionary -Password $GeneralPassword
        #KeePass
        Edit-KPGeneralPassword $GeneralPassword | out-null
        $global:InputPassword = $GeneralPassword
        
}

Function global:Save-Dictionary{
        <#
            .SYNOPSIS
                Saves Dictionary to file with secrets.
            .DESCRIPTION
                Save-Dictionary function saves list of secrets (Dictionary) to entered file in xml format.
                Then file is encrypted by Encription-Dictionary function.
            .PARAMETER Path
                Specifies the path to file with secrets.  
            .EXAMPLE
                PS> Save-Dictionary
            .LINK
                Encrypt-Dictionary
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param(
        [String] $Path = $global:DictionaryPath
        )
        $global:Dictionary | Export-Clixml -Path $Path -Encoding UTF8
        Encrypt-Dictionary -Password $global:InputPassword
}

Function global:Get-Password{
        <#
            .SYNOPSIS
                Gets password from secure string.
            .DESCRIPTION
                The Get-Password function gets entered password (secure string) as plain text.   
            .PARAMETER SecurePassword
                Specifies the password in secure form.
            .EXAMPLE
                PS> Get-Password -SecurePassword (Get-SecretList -Title "Password for Google").SecretValue
            .LINK
                Get-SecretList, Get-SecretValue
            .NOTES
                Author: Viacheslav Frolov
                Telegram: @Viacheslav_Frolov
                GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
        #>
        [CmdletBinding()]
        param(
        [Parameter(Mandatory = $true)]
        [System.Security.SecureString] $SecurePassword
        )
        $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($SecurePassword)
        $PlainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
        return $PlainPassword
}