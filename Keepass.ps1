Using module .\Class.psm1
Using module .\Functions.psm1


Function global:Get-KPSecretValue{
    <#
        .SYNOPSIS
            Gets passord field from KeePass database.
        .DESCRIPTION
            The Get-KPSecretValue gets password field of entered secret title.
            Function use KPScript.
        .PARAMETER Title
            Specifies the title of secret.
        .EXAMPLE
            PS> Get-KPSecretValue -Title "Password for Google"
        .LINK
            Get-KPSecretList, Add-KPSecret, Edit-KPSecret, Remove-KPSecret, Edit-KPGeneralPassword
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
    #>
    [CmdletBinding()]
    param(
    [Parameter(Mandatory = $true)]
    [String] $Title
    )
    $fileExe = $global:KPScriptPath
    $CONFIGURATIONFILE = $global:KPScriptPath + ".config"
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:GetEntryString $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -ref-Title:$Title `
    -Field:Password
}

Function global:Get-KPSecretList{
    <#
        .SYNOPSIS
            Gets list of secret from KeePass database.
        .DESCRIPTION
            The Get-KPSecretList gets list of secret witch match entered title or name or URL.
            Function use KPScript.
        .PARAMETER Title
            Specifies the title of secret.
        .PARAMETER Name
            Specifies the user name of secret.
        .PARAMETER URL
            Specifies the URL of secret.
        .EXAMPLE
            PS> Get-KPSecretList -Title "Password for Google"
            UUID: 93F846BAAEE49546A99E53787BA7DC9E
            GRPU: 21292F60D1F9794E99172E88637DB77D
            GRPN: Database
            S: Notes = 
            S: Password = gfnghmnhmhcgdmy4455
            S: Title = Password for Google
            S: URL = www.rome.it
            S: UserName = Rimus
            TC: 2019-08-16T16:49:54Z
            TLM: 2019-08-24T10:32:05Z
            TE: 2019-11-10T10:06:23Z
            EXP: True

            OK: Operation completed successfully.
        .EXAMPLE
            PS>Get-KPSecretList -Name root
            UUID: C9531F249CF79A4DB77F2B5F16682D42
            GRPU: 21292F60D1F9794E99172E88637DB77D
            GRPN: Database
            S: Notes = 
            S: Password = d50d9fd00acf797ac409d5890fcc76669b727e63
            S: Title = Token for Yandex
            S: URL = www.yandex.ru
            S: UserName = root
            TC: 2019-08-16T16:56:12Z
            TLM: 2019-08-16T16:58:40Z
            TE: 2019-09-13T16:31:00Z
            EXP: True

            OK: Operation completed successfully.
        .EXAMPLE
            PS>Get-KPSecretList -URL www.telegram.com
            UUID: 685D08454A21994B8CED7F2B0DA41450
            GRPU: 21292F60D1F9794E99172E88637DB77D
            GRPN: Database
            S: Notes = 
            S: Password = ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAukDVDC9cSktXTBKVxM1Ddaa6RHkDefRyHGaR1bcbFUVFlASXTMx8SvzSdoRUgQhNF9rKuAiAWCliZbYJLMd97jnQHk1Bq1yFSpxEFmVQWt+/fTp500QH5uBYY0T/6sj1okM/
            txDr/GHogIkM4lUDsfXn6VhYkgEebx2MtkdGj2yH2aod8OJdeCMgVvj/n5zpETqVX0WsNqv9UbSpEwj8Q2U8puMqnS74hyxBEjiGdiBY1i4sAVgg1NUXfWLNfZ0BoFiOQ+2lCxEUrMz+M29eqm+WWCc6vTrhel0bIgt/vTcEi8xx1rjGOb077A
            Sd7uTBlRqIDnnvbidNh9/mpNgPpQ==
            S: Title = Key for telegram
            S: URL = www.telegram.com
            S: UserName = Slava
            TC: 2019-08-16T17:00:16Z
            TLM: 2019-08-16T17:03:46Z
            TE: 2019-09-13T16:31:00Z
            EXP: True

            OK: Operation completed successfully.
        .LINK
            Get-KPValue, Add-KPSecret, Edit-KPSecret, Remove-KPSecret, Edit-KPGeneralPassword
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
    #>
    [CmdletBinding()]
    param(
    [String] $Title,
    [String] $Name,
    [String] $URL
    )
    $fileExe = $global:KPScriptPath
    $CONFIGURATIONFILE = $global:KPScriptPath + ".config"

    if($Name){
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:ListEntries $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -ref-UserName:$Name
    }

    if($Title){
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:ListEntries $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -ref-Title:$Title
    }

    if($URL){
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:ListEntries $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -ref-URL:$URL
    }
}

Function global:Add-KPSecret{
    <#
        .SYNOPSIS
            Adds secret to KeePass database.
        .DESCRIPTION
            The Add-KPSecret adds secret to KeePass database and fills fields:
            "Title", "User name", "Password", "URL", "Expires". 
            However, it is not necessary to enter values of all fields. Them values are set by default.
            Function use KPScript.
        .PARAMETER Title
            Specifies the title of secret.
        .PARAMETER Name
            Specifies the user name of secret.
        .PARAMETER SecretValue
            Specifies the password of secret.
        .PARAMETER URL
            Specifies the URL of secret.
        .PARAMETER Days
            Specifies the expires of secret in days.
        .EXAMPLE
            PS> Add-KPSecret -Title "Password for Google" -Name "Slava" -SecretValue "123" -URL "www.google.com" -Days 30
        .EXAMPLE
            PS> Add-KPSecret
        .LINK
            Get-KPValue, Get-KPSecretList, Edit-KPSecret, Remove-KPSecret, Edit-KPGeneralPassword
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
    #>
    [CmdletBinding()]
    param(
    [String] $Title = "KPSecret",
    [String] $Name = "Slava",
    [String] $SecretValue = "123",
    [String] $URL = "www.gmail.com",
    [int] $Days = 30
    )
    $fileExe = $global:KPScriptPath
    $CONFIGURATIONFILE = $global:KPScriptPath + ".config"
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:AddEntry $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -Title:$Title -UserName:$Name -Password:$SecretValue -URL:$URL -setx-Expires:true -setx-ExpiryTime:(Get-Date).AddDays($Days)
}

Function global:Edit-KPSecret{
      <#
        .SYNOPSIS
            Edits secret of KeePass database.
        .DESCRIPTION
            The Edit-KPSecret edits secret of KeePass database ("Title", "User name", "Password", "URL", "Expires"). 
            Secret is found by entered title. Function use KPScript.
        .PARAMETER Title
            Specifies the title of secret.
        .PARAMETER Name
            Specifies new user name of secret.
        .PARAMETER SecretValue
            Specifies new password of secret.
        .PARAMETER URL
            Specifies new URL of secret.
        .PARAMETER Days
            Specifies new expires of secret.
        .EXAMPLE
            PS> Edit-KPSecret -Title "Password for Google" -Name "Slava" -SecretValue "123" -URL "www.google.com" -ExpiresTime (Get-Date).AddDays(30)
        .LINK
            Get-KPValue, Get-KPSecretList, Add-KPSecret, Remove-KPSecret, Edit-KPGeneralPassword
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
    #>
    [CmdletBinding()]
    param(
    [Parameter(Mandatory = $true)]
    [String] $Title,
    [String] $Name,
    [String] $SecretValue,
    [String] $URL,
    [datetime] $ExpiresTime
    )
    $fileExe = $global:KPScriptPath
    $CONFIGURATIONFILE = $global:KPScriptPath + ".config"
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:EditEntry $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -ref-Title:$Title `
    -set-UserName:$Name -set-Password:$SecretValue -set-URL:$URL -setx-ExpiryTime:$ExpiresTime
}

Function global:Remove-KPSecret{
    <#
        .SYNOPSIS
            Removes secret from KeePass database.
        .DESCRIPTION
            The Remove-KPSecret removes secret from KeePass database which match entered title.
            Function use KPScript.
        .PARAMETER Title
            Specifies the title of secret.
        .EXAMPLE
            PS> Remove-KPSecret -Title "Password for Google"
        .LINK
            Get-KPValue, Get-KPSecretList, Add-KPSecret, Edit-KPSecret, Edit-KPGeneralPassword
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
    #>
    param(
    [Parameter(Mandatory = $true)]
    [String] $Title
    )
    [CmdletBinding()]
    $fileExe = $global:KPScriptPath
    $CONFIGURATIONFILE = $global:KPScriptPath + ".config"
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:DeleteEntry $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -ref-Title:$Title
}

Function global:Edit-KPGeneralPassword{
    <#
        .SYNOPSIS
            Edits Master key of KeePass database.
        .DESCRIPTION
            The Edit-KPGeneralPassword edits Master key (password) of KeePass database.
            Function use KPScript.
        .PARAMETER Password
            Specifies the new password of secret in secure form.
        .EXAMPLE
            PS> Edit-KPGeneralPassword -Password $Password
        .LINK
            Get-KPValue, Get-KPSecretList, Add-KPSecret, Edit-KPSecret, Remove-KPSecret
        .NOTES
            Author: Viacheslav Frolov
            Telegram: @Viacheslav_Frolov
            GitLab: https://gitlab.com/Frolov-Viacheslav/Dictionary
    #>
    param(
    [Parameter(Mandatory = $true)]
    [System.Security.SecureString] $Password
    )
    [CmdletBinding()]
    $fileExe = $global:KPScriptPath
    $CONFIGURATIONFILE = $global:KPScriptPath + ".config"
    & $fileExe  /CONFIGURATIONFILE=$CONFIGURATIONFILE `
    -c:ChangeMasterKey $global:KeePassDatabasePath `
    -pw:(get-password $global:InputPassword) `
    -newpw:(Get-Password $Password)
}

