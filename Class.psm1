﻿Class Secret
{
    # Properties
    [String] $Title = "Secret 1"
    [String] $SecretType = "credential"
    [String] $Name = "User"
    [System.Security.SecureString] $SecretValue = ("123" | ConvertTo-SecureString -AsPlainText -Force)
    [String] $URL = "/home/User"
    [String[]] $Tag = "tag1", "tag2"
    [datetime] $ExpiresTime = (Get-Date).AddDays(30)


    #Hidden properties
    hidden  $PasswordHistory = @()

    # Constructors
    Secret ([String] $Title, [String]$SecretType, [String] $Name, [System.Security.SecureString] $SecretValue, [String] $URL, [String[]] $Tag, [int] $Days)
    {
       $this.Title = $Title;
       $this.SecretType = $SecretType;
       $this.Name = $Name;
       $this.SecretValue = $SecretValue;
       if($this.SecretType -eq "credential"){
       $this.PasswordHistory += $SecretValue;
       }
       $this.URL = $URL;
       $this.Tag = $Tag;
       $this.ExpiresTime = (Get-Date).AddDays($Days);
    }
    hidden Secret ([String] $Title, [String]$SecretType, [String] $Name, [System.Security.SecureString] $SecretValue, [String] $URL, [String[]] $Tag, [int] $Days,  $PasswordHistory)
    {
       $this.Title = $Title;
       $this.SecretType = $SecretType;
       $this.Name = $Name;
       $this.SecretValue = $SecretValue;
       if($this.SecretType -eq "credential"){
       $this.PasswordHistory = @($PasswordHistory);
       }
       $this.URL = $URL;
       $this.Tag = $Tag;
       $this.ExpiresTime = (Get-Date).AddDays($Days);
    }
    Secret ()
    {
       $this.PasswordHistory += $this.SecretValue
    }
}