# <img src="images/logo.jpg" alt="logo" width="120"/> Powershell Database of secrets
Secrets are stored in file *.secret. Each element is an object with following fields:
* Title (unique value of title is advisable)
* SecretType (credential, token, SSH_key)
* Name (Login)
* SecretValue (password, secret value, value of key)
* URL
* Tag (any number of tags)
* ExpiresTime

Example of secret:  

| Field | Value |
| ------ | ------ | 
| Title | Password for Google |
| SecretType | credential | 
| Name | Name |
| SecretValue | System.Security.SecureString | 
| URL | www.g.co |
| Tag | {google, Name password}| 
| ExpiresTime | 9/19/2019 10:53:08 PM | 

File is encrypted by AES-256. Key is password hash which you have to input at first time.  
Then it is stored in file Password. Each time when you use Dictionary you have to enter the password.  
The file with secrets is enccrypted after each operation with it (add, edit, remove or get secret).  
# Prerequisites 
* [Install PowerShell 5.0 or above](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-windows-powershell?view=powershell-6)  
* [Install KeePass 2.42.1 or above](https://keepass.info/download.html)  
* [Install KPScript](https://keepass.info/plugins.html#kpscript)  
# Getting start with program
1. Create Keepass database of secrets.  
2. Download files: Class.psm1, Functions.psm1, KeePass.ps1, UI.ps1.  
3. Customize environment (DictionaryPath, GeneralPasswordPath, KeePassDatabasePath, KPScriptPath).  
4. Run .\UI.ps1.  
5. Set up the General password (same as in the KeePass database).    
6. Type the name of the desired function.  
# Program has some functions:
1. Get value of secret by title  
If parameter KeePass is specified, output is got from KeePass database, else - from Dictionary.  
`PS> Get-SecretValue -Title "Password for Google"`
2. Get list of secrets by title or name or URL or expires time  
If parameter KeePass is specified, output is got from KeePass database, else - from Dictionary.    
`PS> Get-SecretList -Title "Password for Google"`  
`PS> Get-SecretList -Name "root"`  
`PS> Get-SecretList -URL "www.google.com"`  
`PS> Get-SecretList -Days 30`
3. Edit secret by title  
`PS> Edit-Secret -Title "Password for Google" -NewName "Name" -NewSecretValue -NewURL "www.g.co" -NewDays 30 -NewTag "google"`  
4. Remove secret by value  
`PS> Remove-Secret -Title "Password for Google"`  
5. Generate password by pattern:
* A - capital latter
* a - small latter
* D - digit
* #number - length of password
* %(A,a,D)[number] - format string in the start of password (number - count of symbol).  
If number is not specified, count of symbols equal one.  
Example 1: #5AaD - I946y  
Example 2: #6DAa%D2a3A1 - 17luzY  
Example 3: #7a%D5 - 23962og  
Example 4: #10aAD%A3D2a1 - JWT53wfrUU  
`PS> Generate-Password -Pattern "#30ADa%D10A10a5DDA"`  
6. Add secret  
`PS> Add-Secret -Title "Test" -SecretType token -Name "Name" -URL "Test.com" -Tag "Test" -Days "30"`  
7. Expand expires time of secret  
`PS> Get-SecretList -Title "Password for Google" | Expand-ExpiresTime -Days 100`  
8. Add tag to secret  
`PS> Get-SecretList -Title "Password for Google" | Add-Tag -NewTag ("Tag1", "Tag2")`  
9. Restore password from password history  
`PS> Get-SecretList -Title "Password for Google" | Restore-Password -Index 2`  
10. Get password history of secret  
`PS> Get-SecretList -Title "Password for Google" | Get-PasswordHistory`  
11. Encrypt Database of secrets  
`PS> Encrypt-Dictionary -Password "123"`  
12. Decrypt Database of secrets  
`PS> Decrypt-Dictionary -Password "123"`  
13. Set general password of Database of secrets  
`PS> Set-GeneralPassword`  
14. Edit general password of Database of secrets  
`PS> Edit-GeneralPassword`  

# Program can work with KeePass through next functions:
1. Get value of secret  
`PS> Get-KPSecretValue -Title "Password for Google"`  
2. Get list of secrets by title or name or URL  
`PS> Get-KPSecretList -Title "Password for Google"`  
`PS>Get-KPSecretList -Name root`  
`PS>Get-KPSecretList -URL www.telegram.com`  
3. Add secret  
`PS> Add-KPSecret -Title "Password for Google" -Name "Slava" -SecretValue "123" -URL "www.google.com" -Days 30`  
`PS> Add-KPSecret`  
4. Edit secret  
`PS> Edit-KPSecret -Title "Password for Google" -Name "Slava" -SecretValue "123" -URL "www.google.com" -ExpiresTime (Get-Date).AddDays(30)`  
5. Remove secret  
`PS> Remove-KPSecret -Title "Password for Google"`
6. Edit general password of Database of secrets  
`PS> Edit-KPGeneralPassword -Password $Password`  

# Authors  
Viacheslav Frolov, DevOps Engineer  
Telegram: @Viacheslav_Frolov